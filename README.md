# Drying Rack

We acquired a folding rack that was missing the feet required to
stablize it, and I iterated my way to DryingRackFull.scad, from which
two feet were printed and work pretty well, though there's room for
improvement.
