// Written by Kevin Cole <kevin.cole@novawebcoop.org> 2016.08.11
//
// Drying rack supports.
//
// First measurements (top) 2016.08.08
// Tube diameter ~ 14.35 mm and sits ~ 21.00 mm off
// the ground. Altogether both tubes are ~ 42 mm tall.
//
// Second measurements (bottom) 2016.08.11
// Tube diameter ~ 14.57 mm and sits ~ 24.11 mm off the ground
// (height of lower tube sheath: 21.04 with 0.5 height of 
// upper tube sheath minus the tube (20.71 - 14.57) / 2)
//

translate([0, 0, 7.285])
union() {
  difference() {
    // Main body
    cube([50, 31.395, 15.6975], true);  // ([w, d, h], center)
    // Tube trough
    rotate(a=[0, 90, 0])
    translate([-7.285, 0, -25])  // (31.395/2, 0, 50/2)
    cylinder(50, 7.285, 7.285);
  };
    // Screw peg
    translate([18, 11.773125, 0])
    cylinder(15.6975, 1.5, 1.5);
    // Screw peg
    translate([-18, 11.773125, 0])
    cylinder(15.6975, 1.5, 1.5);
    // Screw peg
    translate([18, -11.773125, 0])
    cylinder(15.6975, 1.5, 1.5);
    // Screw peg
    translate([-18, -11.773125, 0])
    cylinder(15.6975, 1.5, 1.5);
};
